# FATUnicorn Android Test #
### Prerequisites ###
* This is a coding proficiency test. You must be able to perform these 3 basic tasks on a day to day basis at FATUnicorn
* All apps must work for Normal screens, Large screens and extra large screens
* All apps must work for Low density (120) ldpi, Medium density (160) mdpi, High density (240) hdpi, Extra-high-density (320) xhdpi
* All apps must work with Lollipop (5.0) and upwards
* The app must be able to be compiled in Android Studio 2.2.2.0 (osx)

### Instructions ###
* Fork this project
* Complete the test
* Commit and push to your repo
* Make a pull request to our repo
* Email neilj@fatunicorn.com with a link to your pull request

### Networking ###
* Create a new Android project in Tests/Networking folder
* Make a URL request to https://bitbucket.org/fatunicorn/android-test/raw/c5ba5f96ab6642ca2f72a7805b09094d63618157/Data/people-data.json
* Store the data in an SQL database
* Display the data in a ViewPager
* Create a downloadable .apk and name it 'networking'
* Commit and push your code, tagging the commit as 'networking'

### Camera ###
* Create a new Android project in Tests/Camera folder
* Have the ability to take a selfie. 
* Display the selfie, and provied options to save or retake
* Add a custom camera shutter button for extra merit
* Create a downloadable .apk and name it 'camera'
* Commit and push your code, tagging the commit as 'camera'

### Layout ###
* Create a new Android project in Tests/Layout folder
* Create a long form (i.e. page containing editable information 'name', 'email', password) that extends larger than an extra large screen's height'.
* Make sure that when the user edits any of the EditTexts the input is always visible, in both landscape and portrait orientations.
* Create a downloadable .apk and name it 'layout'
* Commit and push your code, tagging the commit as 'layout'.